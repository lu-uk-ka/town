# Town

Project TODO.md file as a versioned issue tracker.

### Todo

  - [ ] #3: add a wp-json backend @major @proposal

    for simple CMS functionality; see https://developer.wordpress.org/wp-json/wp/v2

  - [ ] #7: map archival logic @major @enhancement @v0.2

    keep snapshots of map deltas in order to provide "archival" states of map(?s)

### Doing

 - [ ] ?

### Done

 - [ ] ?
